'use strict'

const path = require('path');
const yargs = require('yargs');
const gulp = require('gulp');
const rimraf = require('rimraf');
const fs = require('fs');
const browser = require('browser-sync');
const yaml = require('js-yaml');
const plugins = require('gulp-load-plugins');
const named = require('vinyl-named');
const webpackStream = require('webpack-stream');
const webpack = webpackStream.webpack;
const gulplog = require('gulplog');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const time = Date.now();
const panini = require('panini');


const $ = plugins();
const PRODUCTION = !!(yargs.argv.production);
const { COMPATIBILITY, PORT, UNCSS_OPTIONS, PATHS } = loadConfig();

let webpackConfig = {
	output: {
		publicPath: PATHS.dist+'/js',
	},
	module: {
		loaders: [{
			test:    /\.js$/,
			loader:  'babel-loader?presets[]=env'
		}]
	},
	plugins: [
	new webpack.NoErrorsPlugin(),
	new webpack.ProvidePlugin({
		$: 'jquery',
		jQuery: 'jquery'
	}),
	new webpack.optimize.CommonsChunkPlugin({
		name: 'common'
	})
	]
};

if (!PRODUCTION) {
	webpackConfig.watch = true;
	webpackConfig.devtool = 'cheap-module-inline-source-map'
} else {
	webpackConfig.watch = false;
	webpackConfig.devtool = false;
	webpackConfig.plugins.push(new UglifyJsPlugin())
}

function loadConfig() {
	let ymlFile = fs.readFileSync('config.yml', 'utf8');
	return yaml.load(ymlFile);
}


gulp.task('styles', function() {
	return gulp.src('src/assets/scss/*.scss')
	.pipe($.sourcemaps.init())
	.pipe($.sass({
		includePaths: PATHS.sass
	})
	.on('error', $.sass.logError))
	.pipe($.autoprefixer({
		browsers: COMPATIBILITY
	}))
    // Comment in the pipe below to run UnCSS in production
    //.pipe($.if(PRODUCTION, $.uncss(UNCSS_OPTIONS)))
    .pipe($.if(PRODUCTION, $.cleanCss({ compatibility: 'ie9' })))
    .pipe($.if(!PRODUCTION, $.sourcemaps.write()))
    .pipe($.rename(function (path) {
    	path.basename += `-${time}`;
    }))
    .pipe(gulp.dest(PATHS.dist+'/assets/css'))
});

gulp.task('images', function() {
	return gulp.src('src/assets/img/**/*')
	.pipe($.if(PRODUCTION, $.imagemin({
		progressive: true
	})))
	.pipe(gulp.dest(PATHS.dist+'/assets/img'));
});

gulp.task('pages', function() {
	return gulp.src('src/pages/**/*.{html,hbs,handlebars,php}')
	.pipe(panini({
		root: 'src/pages/',
		layouts: 'src/layouts/',
		partials: 'src/partials/',
		data: 'src/data/',
		helpers: 'src/helpers/'
	}))
	.pipe($.htmlReplace({
		'js': [
		`<script src="/assets/js/common-${time}.js"></script>`
		],
		'css': [
		`<link rel="stylesheet" href="/assets/css/app-${time}.css">`
		]
	}))
	.pipe(gulp.dest(PATHS.dist));
});

// Load updated HTML templates and partials into Panini
gulp.task('resetPages', function(done) {
	panini.refresh();
	done();
});

gulp.task('webpack', function(callback) {
	let firstBuildReady = false;

	function done(err, stats) {
		firstBuildReady = true;
		if (err) {
			return;
		}
		gulplog[stats.hasErrors() ? 'error' : 'info'](stats.toString({
			assets: true,
			chunks: false,
			chunkModules: false,
			colors: true,
			hash: false,
			timings: true,
			version: false
		}));
	}

	return gulp.src(PATHS.entries)
	.pipe($.plumber({
		errorHandler: $.notify.onError(err => ({
			title:   'Webpack',
			message: err.message
		}))
	}))
	.pipe(named())
	.pipe(webpackStream(webpackConfig, null, done))
	.pipe($.rename(function (path) {
		path.basename += `-${time}`;
	}))
	.pipe(gulp.dest(PATHS.dist+'/assets/js'))
	.on('data', () => firstBuildReady && callback())
})

function server(done) {
	browser.init({
		server: PATHS.dist,
		port: PORT
	});
	done();
}

function reload(done) {
	browser.reload();
	done();
}

function clean(done) {
	rimraf(PATHS.dist, done);
}

function copy() {
	return gulp.src(PATHS.assets)
	.pipe(gulp.dest(PATHS.dist+'/assets'));
}

function watch() {
	gulp.watch(PATHS.assets, copy);
	gulp.watch('src/pages/**/*.*').on('all', gulp.series('pages', browser.reload));
	gulp.watch('src/{layouts,partials}/**/*.html').on('all', gulp.series('resetPages', 'pages', browser.reload));
	gulp.watch('src/assets/scss/**/*.scss').on('all',  gulp.series('styles', browser.reload));
	gulp.watch('src/assets/img/**/*').on('all', gulp.series('images', browser.reload));
	gulp.watch(PATHS.dist+'/assets/js/**/*').on('all', browser.reload);
}



gulp.task('build',
          gulp.series(clean, gulp.parallel('styles', 'webpack', 'images', copy, 'pages')));


gulp.task ('default',
           gulp.series('build', server, watch));

